#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct employee{
	int count;
	char ename[20];
	float sal;
	struct employee *next;
}emp;
void main(){
	emp*emp1=(emp*)malloc(sizeof(emp));
	emp*emp2=(emp*)malloc(sizeof(emp));
	emp*emp3=(emp*)malloc(sizeof(emp));
	emp1->count=50;
	strcpy(emp1->ename,"Persistent");
	emp1->sal=50.90;
	emp1->next=emp2;
	printf("%d\n",emp1->count);
	printf("%s\n",emp1->ename);
	printf("%f\n",emp1->sal);
	printf("%p\n",emp1->next);

	emp2->count=100;
	strcpy(emp2->ename,"Microsoft");
	emp2->sal=90.89;
	emp2->next=emp3;
	printf("%d\n",emp2->count);             
	printf("%s\n",emp2->ename);     
	printf("%f\n",emp2->sal);
	printf("%p\n",emp2->next);

	emp3->count=150;
	strcpy(emp3->ename,"TCS");
	emp3->sal=9676.89;
	emp3->next=NULL;

	printf("%d\n",emp3->count);          
	printf("%s\n",emp3->ename);   
	printf("%f\n",emp3->sal);     
	printf("%p\n",emp3->next);
}
	
