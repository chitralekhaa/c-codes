#include<stdio.h>
#include<string.h>
typedef struct employee{
	int empID;
	char empName[20];
	float sal;
	struct employee *next;
}emp;
void main(){
	emp obj1,obj2,obj3;
	emp *head=&obj1;
	head->empID=1;
	strcpy(head->empName,"rohit");
	head->sal=99.90;
	head->next=&obj2;
	printf("%d\n",head->empID);
	printf("%s\n",head->empName);
	printf("%f\n",head->sal);
	printf("%p\n",head->next);
	head->next->empID=2;
	strcpy(head->next->empName,"rahul");
	head->next->sal=89.90;
	head->next->next=&obj3;
	printf("%d\n",head->next->empID);
	printf("%s\n",head->next->empName);     
	printf("%f\n",head->next->sal);    
	printf("%p\n",head->next->next);
	head->next->next->empID=3;
	strcpy(head->next->next->empName,"radha");
	head->next->next->sal=90.90;
	head->next->next->next=NULL;
	printf("%d\n",head->next->next->empID); 
	printf("%s\n",head->next->next->empName);    
	printf("%f\n",head->next->next->sal);   
	printf("%p\n",head->next->next->next); 
}

