#include<stdio.h>
#include<string.h>
typedef struct company{
       int count;
       char name[20];
       float rev;
       struct company *next;
}comp;
void main(){
	comp obj1,obj2,obj3;
	comp *head=&obj1;
	head->count=100;
	strcpy(head->name,"Google");
	head->rev=56.89;
	head->next=&obj2;
	printf("%d\n",head->count); 
	printf("%s\n",head->name);
	printf("%f\n",head->rev); 
	printf("%p\n",head->next); 
	head->next->count=200;
	strcpy(head->next->name,"Meta");
	head->next->rev=34.56;
	head->next->next=&obj3;
	printf("%d\n",head->next->count); 
	printf("%s\n",head->next->name); 
	printf("%f\n",head->next->rev);
	printf("%p\n",head->next->next);
	head->next->next->count=900;
	strcpy(head->next->next->name,"Netflix");
	head->next->next->rev=90.78;
	head->next->next->next=NULL;
       	printf("%d\n",head->next->next->count);
	printf("%s\n",head->next->next->name);   
	printf("%f\n",head->next->next->rev);    
	printf("%p\n",head->next->next->next); 
}	
          	

