//stack using linked list

#include<stdio.h>
#include<stdlib.h>
typedef struct node{
	int data;
	struct node *next;
}node;
node *head=NULL;
node* createnode(){
	node* newnode=(node*)malloc(sizeof(node));
	printf("enter data:");
	scanf("%d",&newnode->data);
	newnode->next=NULL;
}
int count();
int push(int n){
	int c=count();
	if(c>n){
		printf("stack overflow\n");
		return -1;
	}else{
		node *newnode=createnode();
		if(head==NULL){
			head=newnode;
		}else{
			node *temp=head;
			while(temp->next!=NULL){
				temp=temp->next;
			}temp->next=newnode;
		}return 0;
	}
}int peek(){
	if(head==NULL){
		printf("stack is empty\n");
	}else{
		node *temp=head;
		while(temp->next!=0){
			temp=temp->next;
		}printf("%d",temp->data);
		return 0;
	}
}
int pop(){
	if(head==NULL){
		printf("stack underflow\n");
		return -1;
	}
	else{
		node* temp=head;
		while(temp->next->next!=0){
			temp=temp->next;
		}free(temp->next);
		temp->next=NULL;
		return 0;
	}
}
int count(){
	int c=1;
	node *temp=head;
	while(temp!=0){
		c++;
		temp=temp->next;
	}return c;
}
void main(){ 
        int no;
	printf("enter no of stacks:");	
	scanf("%d",&no);
	char ch;                              
	do{                     
		printf("1.push.\n");      
		printf("2.pop.\n");       
		printf("3.peek.\n");          
		int choice;         
		printf("enter choice:\n");      
		scanf("%d",&choice);         
		switch(choice){          
			case 1:{      	       
				       push(no);        
				       break;                    
			       }                             
			case 2:{               
				       pop();           
				       break;        
			       }       
			case 3:        
			       peek();       
			       break;                      
			default:            
			       printf("wrong input\n");   
		}getchar();          
		printf("Do You Want To Continue??\n");      
		scanf("%c",&ch);       
	}while(ch=='y'||ch=='Y');                                                                                                        }                                  
