// implementing stack using linked list
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
typedef struct demo{
	int data;
	struct demo *next;
}demo;
int stack;
int flag=0;
demo *head=NULL;
int countNode(){
	int c=0;
	demo*temp=head;
	while(temp!=0){
		c++;
		temp=temp->next;
	}return c;
}
bool isfull(){
	if(countNode()==stack){
		return true;
	}else{
		return false;
	}
}
demo* CreateNode(){
	demo*newnode=(demo*)malloc(sizeof(demo));
	printf("enter data:");
	scanf("%d",&newnode->data);
	newnode->next=NULL;
}
void AddNode(){
	demo *newnode=CreateNode();
	if(head==NULL){
		head=newnode;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}temp->next=newnode;
	}
}
int push(){
	if(isfull()){
		return -1;
	}else{
		AddNode();
		return 0;

	}
}bool isempty(){
	if(head==NULL){
		return true;
	}else{
		return false;
	}
}
int DeleteLast(){
	demo *temp=head;
	while(temp->next!=0){
	
		temp=temp->next;
	}int val=temp->data;
	free(temp->next);
	temp->next=NULL;
	return val;
}
int pop(){
	if(isempty()){
		flag=1;
		return -1;
	}else{
		demo *temp=head;     
		while(temp->next!=0){      
			temp=temp->next;      
		}flag=0;
		int val=temp->data;      
		free(temp->next);     
		temp->next=NULL;            
		return val; 

	}

}
int stack;
void main(){
	printf("enter no of stacks:");
	scanf("%d",&stack);
	char ch;
	do{
		printf("1.push\n");				
		printf("2.pop\n");
		printf("3.peek\n");
		printf("4.isempty\n");
		printf("5.isfull\n");
		int choice;
		printf("enter choice:");
		scanf("%d",&choice);
		switch(choice){
			case 1:{
				       int ret=push();
				       if(ret==-1){
					       printf("stack overflow\n");
				       }
				      }
				break;
			case 2:{
				       int ret=pop();
				       if(flag==1){
					       printf("stack is empty\n");
				       }else{
					       printf("%d is popped element\n",ret);
				       }
			       }
			       break;
		/*	case 3:{
				       int ret=peek(stack);
				       if(flag==0){
					       printf("stack is empty\n");
				       }else{
					       printf("%d is at top\n",ret);
				       }
			       }
			       break;
			case 4:{
				       if(isempty()){
					       printf("stack is empty!!\n");
				       }else{
					       printf("stack is not empty\n");
				       }
			       }
			       break;
			case 5:{
				      
				       if(isfull()){									
					       printf("stack is full!!\n");																	
				       }else{								
					       printf("stack is not full\n");
       				       }				 
			       }break;				*/							
			default:
			       printf("wrong choice\n");
		}getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&ch);
	}while(ch=='Y' || ch=='y');
}
		
