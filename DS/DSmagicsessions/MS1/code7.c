//print the maximum integer data from the above code
#include<stdio.h>
#include<stdlib.h>
typedef struct demo{
	int data;
	struct demo *next;
}demo;
demo *head=NULL;
void AddNode(){
	demo *newnode=(demo*)malloc(sizeof(demo));
	getchar();
	printf("enter data:");
	scanf("%d",&newnode->data);
	newnode->next=NULL;
	if(head==NULL){
		head=newnode;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}temp->next=newnode;
	}
}void maximum(){
	demo *temp=head;
	int max=temp->data;
	while(temp!=NULL){
		if(temp->data>=max)
			max=temp->data;
		temp=temp->next;
	}printf("%d",max);
}
void main(){
	int node;
	printf("enter no of nodes:");
	scanf("%d",&node);
	for(int i=0;i<node;i++){
		AddNode();
	}maximum();
}
