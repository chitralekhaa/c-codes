/* WAP to check the prime number present in the data from the above nodes */

#include<stdio.h>
#include<stdlib.h>
typedef struct demo{
	int data;
	struct demo *next;
}demo;
demo *head=NULL;
void AddNode(){
	demo *newnode=(demo*)malloc(sizeof(demo));
	getchar();
	printf("enter data:");
	scanf("%d",&newnode->data);
	newnode->next=NULL;
	if(head==NULL){
		head=newnode;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}temp->next=newnode;
	}
}void primeNos(){
	demo *temp=head;
	int flag=0,count=0;
	while(temp!=NULL){
		if(temp->data==0 || temp->data==1){
			flag = 1;
		}
		for(int i=2;i<=temp->data/2;i++){
			if(temp->data%i==0){
				flag=1;
				break;
				
			}
		}if(flag==0){
			count++;
			break;
		}
		
		
		temp=temp->next;
	}
	if(count==0){
		printf("prime numbers not present");
	}else{
		printf("prime numbers present");
	}
}
void main(){
	int node;
	printf("enter no of nodes:");
	scanf("%d",&node);
	for(int i=0;i<node;i++){
		AddNode();
	}primeNos();
	printf("\n");
	
}
