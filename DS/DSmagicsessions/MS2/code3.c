//WAP that searches the occurence of particular element from SLL
#include<stdio.h>
#include<stdlib.h>
typedef struct node{
	int data;
	struct node *next;
}node;
node* head=NULL;
node* createnode(){
	node* newnode=(node*)malloc(sizeof(node));
	printf("enter data:");
	scanf("%d",&(newnode->data));
	newnode->next=NULL;
	return newnode;
}
void AddNodes(){
	node *newnode=createnode();
	if(head==NULL){
		head=newnode;
	}else{
		node*temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}temp->next=newnode;
	}
}void occ(int num){
	int count=0;
	node*temp=head;
	while(temp!=NULL){
		if(temp->data==num){
			count++;
		}
		temp=temp->next;
	}printf("%d occured %d times in list",num,count);

}
void main(){
	int nonodes;
	printf("enter number of nodes");
	scanf("%d",&nonodes);
	for(int i=1;i<=nonodes;i++){
		AddNodes();
	}int num;
	printf("enter number:");
	scanf("%d",&num);
	occ(num);
}
