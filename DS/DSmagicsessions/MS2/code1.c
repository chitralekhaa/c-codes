//WAP that searches for the first occurence of particular element from a singly linear linked list
#include<stdio.h>
#include<stdlib.h>
typedef struct demo{
	int data;
	struct demo *next;
}demo
demo *head=NULL;
demo* CreateNode(){
	demo*newnode=(demo*)malloc(sizeof(demo));
	printf("enter data:");
	scanf("%d",&newnode->data);
	newnode->next=NULL;
	return newnode;
}
void AddNode(){
	demo*newnode=CreateNode();
	if(head==NULL){
		head=newnode;
	}else{
		demo* temp=head;
		while(temp->next!=0){
			temp=temp->next;
		}temp->next=newnode;
	}
}void firstocc(int num){
	int count=0;
	demo* temp=head;
	while(temp!=0){
		count++;
		if(num==temp->data){
			printf("%d",count);
			break;
		}
		temp=temp->next;
	}
}
void main(){
	int nodes;
	printf("enter no of nodes:");
	scanf("%d",&nodes);
	for(int i=1;i<=nodes;i++){
		AddNode();
	}int num;
	printf("enter number you want to search:");
	scanf("%d",&num);
	firstocc(num);
}
