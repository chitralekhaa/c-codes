//AddFirst() function
#include<stdio.h>
#include<stdlib.h>
typedef struct demo{
	int data;
	struct demo *next;
}demo;
demo *head=NULL;
demo *AddNode(){
	demo *newnode=(demo*)malloc(sizeof(demo));
	getchar();
	printf("enter data:");
	scanf("%d",&newnode->data);
	newnode->next=NULL;
}
void createnode(){
	demo *newnode=AddNode();
	if(head==NULL){
		head=newnode;
	}else{
		demo *temp=head;
		while(temp -> next !=NULL){
			temp=temp->next;
		}temp->next=newnode;
	}
}
void AddFirst(){
	demo *newnode=AddNode();
	if(head==NULL){
		head=newnode;
	}else{
		newnode->next=head;
		head=newnode;
	}
}void printLL(){
	demo *temp=head;
	while(temp!=0){
		printf("%d",temp->data);
		temp=temp->next;
	}
}
void main(){
	int nodes;
	printf("enter no of nodes:");
	scanf("%d",&nodes);
	for(int i=0;i<nodes;i++){
		createnode();
	}AddFirst();
	printLL();
}
