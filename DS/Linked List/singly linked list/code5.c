// Addatposition() function
#include<stdio.h>
#include<stdlib.h>
typedef struct demo{
	int data;
	struct demo *next;
}demo;
demo *head=NULL;
demo *createnode(){
	demo *newnode=(demo*)malloc(sizeof(demo));
	printf("enter data:");
	scanf("%d",&newnode->data);
	newnode->next=NULL;
	return newnode;
}
void addnode(){
	demo *newnode=createnode();
	if(head==NULL)
		head=newnode;
	else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}temp->next=newnode;
	}
}
void addatposition(int pos){
	demo *newnode=createnode();
	demo *temp=head;
	while(pos-2){
		temp=temp->next;
		pos--;
	}newnode->next=temp->next;
	temp->next=newnode;
}void printll(){
	demo *temp=head;
	while(temp != NULL){
		printf("%d\n",temp->data);
		temp=temp->next;
	}
}
void main(){
	int node;
	printf("enter no of nodes:");
	scanf("%d",&node);
	for(int i=0;i<node;i++){
		addnode();
	}printll();
	int pos;
	printf("enter at which position you want node to be added:");
	scanf("%d",&pos);
	addatposition(pos);
	printll();	
}
