//reverse SLL
#include<stdio.h>
#include<stdlib.h>
struct demo{
	int data;
	struct demo *next;
}demo;
struct demo *head=NULL;
struct demo* CreateNode(){
	struct demo *newnode=(struct demo*)malloc(sizeof(struct demo));
	printf("enter data:");
	scanf("%d",&newnode->data);
	newnode->next=NULL;
	return newnode;
}
void AddNodes(){
	struct demo *newnode=CreateNode();
	if(head==NULL){
		head=newnode;
	}else{
		struct demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}temp->next=newnode;
	}

}int countSLL(){
	struct demo *temp=head;
	int c=0;
	while(temp!=NULL){
		c++;
		temp=temp->next;
	}return c;
}
int rev(){
	if(head==NULL){
		printf("Linked list is empty");
		return -1;
	}else{
		int count=countSLL();
		int c=count/2;
		struct demo *temp1=head;
		struct demo *temp2=head;
		int a=count-1;

		while(c){
		       struct demo *temp2=head;	
			int i=0;
			while(i<a){
				temp2=temp2->next;
				i++;
			}
			int temp=temp2->data;
			temp2->data=temp1->data;
			temp1->data=temp;
			c--;	
			a--;
		        temp1=temp1->next;	
		}return 0;
	}
}
void printll(){
	struct demo*temp=head;
	while(temp!=0){
		printf("%d",temp->data);
		temp=temp->next;
	}
}
void main(){
	int nodes;
	printf("enter no of nodes:");
	scanf("%d",&nodes);
	for(int i=1;i<=nodes;i++){
		AddNodes();
	}
	printf("original data\n");
	printll();
	printf("\n");
	printf("reversed data\n");
	rev();
	printll();
	printf("\n");


}
