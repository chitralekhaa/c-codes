//inplace rev DLL
#include<stdio.h>           
#include<stdlib.h>            
typedef struct demo{   
        struct demo *prev;	
	int data;      
	struct demo *next;       
}demo;       
struct demo *head=NULL;       
struct demo* CreateNode(){    	
	struct demo *newnode=(struct demo*)malloc(sizeof(struct demo));    
        newnode->prev=NULL;	
	printf("enter data:");            
	scanf("%d",&newnode->data);                                  
	newnode->next=NULL;                                                                                                                      return newnode;                                          
}                            
void AddNodes(){      	
	struct demo *newnode=CreateNode();                        
	if(head==NULL){             			
		head=newnode;      				
	}else{           
		struct demo *temp=head;      
		while(temp->next!=NULL){       
			temp=temp->next;        											
		}temp->next=newnode;
		newnode->prev=temp;
	}            
}int countSLL(){       
	struct demo *temp=head;       
	int c=0;         
	while(temp!=NULL){      
		c++;          
		temp=temp->next;        
	}return c;       
}void printll(){               
	struct demo*temp=head;    
	while(temp!=0){     
		printf("%d",temp->data);       
		temp=temp->next;     
	}
}void rev(){
	demo *temp=NULL;
	while(head!=0){
		head->prev=head->next;
		head->next=temp;
		temp=head;
		head=head->prev;
	}head=temp;
}
void main(){        
	int nodes;   
	printf("enter no of nodes:");       
	scanf("%d",&nodes);      
	for(int i=1;i<=nodes;i++){     
			AddNodes();                   
		}            
	printf("original data\n");     
	printll();          
	printf("\n");       
	printf("reversed data\n");   
	rev();     	
	printll(); 	
	printf("\n");    
}    


    
