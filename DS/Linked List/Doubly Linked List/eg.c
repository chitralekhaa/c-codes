#include<stdio.h>
#include<stdlib.h>
struct hospital{
	struct hospital *prev;
	int P_no;
	struct hospital *next;
};
struct hospital *head=NULL;
//Creating Node
struct hospital* CreateNode(){
	struct hospital *newnode=(struct hospital*)malloc(sizeof(struct hospital));
	newnode->prev=NULL;
	printf("Enter Patient NO:");
	scanf("%d",&newnode->P_no);
	newnode->next=NULL;
}//Add Node 
void AddNode(){
	struct hospital *newnode=CreateNode();
	if(head==NULL){
		head=newnode;
	}else{
		struct hospital *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}temp->next=newnode;
		newnode->prev=temp;
	}
}
//AddFirst
void AddFirst(){
	struct hospital *newnode=CreateNode();
	if(head==NULL){
		head=newnode;
	}else{
		newnode->next=head;
		head->prev=newnode;
		head=newnode;
	}
}
//print
void printll(){
	if(head==NULL){
		printf("List is empty");
	}
	else{
		struct hospital *temp=head;
		while(temp->next!=NULL){
			printf("%d\n",temp->P_no);
			temp=temp->next;
		}printf("%d\n",temp->P_no);
	}
}
//count
int count(){
	int c=0;
	struct hospital *temp=head;
	while(temp!=0){
		c++;
		temp=temp->next;
	}return c;
}
//Add At Pos
void AddAtPos(int pos){
	int c=count();
	if(pos<=0 || pos>=c+2){
		printf("Invalid position\n");
	}else{
		if(pos==1)
			AddFirst();
		else if(pos==c+1)
			AddNode();
		else{
			struct hospital *newnode=CreateNode();
			struct hospital *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}newnode->next=temp->next;
			newnode->prev=temp;
			temp->next->prev=newnode;
			temp->next=newnode;
		}
	}
}
//Delete First
int DeleteFirst(){
	if(head==NULL){
		printf("Linked list is empty!!");
		return -1;
	}
	else{ 
		if(head->next==NULL){
			free(head);
			head=NULL;
		}
		else{
			struct hospital *temp=head;
			head=temp->next;
			head->prev=NULL;
			free(temp);
		}
			return 0;
	}
}
//Delete Last
void DeleteLast(){
	if(head==NULL){
		printf("Linked List is empty");
	}else{
		struct hospital *temp=head;
		if(head->next==NULL){
			free(temp);
			head=NULL;
		}else{
			while(temp->next->next!=NULL){
				temp=temp->next;
			}free(temp->next);
			temp->next=NULL;
		}
	}
}
int DeleteAtPos(int pos){
	struct hospital *temp=head;
	int c=count();
	if(pos<=0 || pos>c){
		printf("invalid position");
		return -1;
	}else{
		if(pos==1)
			DeleteFirst();
		else if(pos==c)
			DeleteLast();
	        else{
			while(pos-2){
				temp=temp->next;
				pos--;
			}struct hospital *temp2=temp->next;
			temp->next=temp2->next;
			temp2->next->prev=temp;
			free(temp2);
		}return 0;
	}
}
void main(){               
	char ch;      
	do{           
		printf("1.AddNode.\n");     
		printf("2.printll.\n");        
		printf("3.AddFirst.\n");     
		printf("4.DeleteFirst.\n");    
		printf("5.DeleteLast.\n");        
		printf("6.DeleteAtpos.\n");       
		printf("7.AddAtPos.\n");               
		printf("8.Count.\n");    
		int choice;           
		printf("enter choice:");     
		scanf("%d",&choice);      
		switch(choice){        
			case 1:      
				AddNode();       
				break;       
			case 2:               
				printll();            
				break;            
			case 3:      
				AddFirst(); 
				break;              
			case 4:     
				DeleteFirst();      
				break;     
			case 5:            
				DeleteLast();    
				break;       
			case 6:{         
				       int pos;    
				       printf("enter pos:");   
				       scanf("%d",&pos);        
				       DeleteAtPos(pos);         
			       }       
			       break;     
			case 7:{          
				       int pos;       
				       printf("enter pos:");            
				       scanf("%d",&pos);         
				       AddAtPos(pos);       
			       }        
			       break;        
			case 8:          
			       count();         
			       break;       
			default:         
			       printf("Invalid Choice\n");         
		}getchar();             
		printf("Do You Want To Continue\n");    
		scanf("%c",&ch);      
	}while(ch=='y' ||ch=='Y');       
}                       
				
