#include<stdio.h>
void main(){
	int x=10;
	char ch='A';
	int *ptr1=&x;
	int *ptr2=&ch;//incompatible pointer type 
	printf("%p\n",ptr1);//data type of var and dt of pointer should be same
	printf("%p\n",ptr2);
	printf("%d\n",*ptr1);
	printf("%d\n",*ptr2);
}
