//void pointer(generic pointer
//--->no associated datatype
//--->can hold address of any data type
//--->cannot be dereferenced

#include<stdio.h>
void main(){
	int x=10;
	int *ptr=&x;
	void *vptr=&x;
	printf("%p\n",ptr);
	printf("%p\n",vptr);
	printf("%d\n",*ptr);
	//printf("%d\n",*vptr);//warning dereferencing void pointer
	//invalid use of void expression
	printf("%d\n",*((int*)vptr));//void pointer to be derefernces needs to be typecast
}


