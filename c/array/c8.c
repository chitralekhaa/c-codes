//accessing array elements
#include<stdio.h>
void main(){
	char carr[4];
	carr[0]='a';
	carr[1]='b';
	carr[2]='c';
	carr[3]='d';
	printf("%c\n",carr[0]);
	printf("%c\n",carr[1]);
	printf("%c\n",carr[2]);
	printf("%c\n",carr[3]);
	printf("%c\n",carr[4]);// other languages gives error saying out of range
}
