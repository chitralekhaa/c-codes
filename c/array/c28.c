/*relation between 2D array and pointers*/
#include<stdio.h>
void main(){
	int arr[3][3]={1,2,3,4,5,6,7,8,9};
	printf("%d\n",arr[0][2]);//3
	printf("%d\n",arr[2][1]); //8
	printf("%d\n",arr[1][2]); //6

	printf("%p\n",arr);
	printf("%p\n",arr[0]);
	printf("%p\n",arr[1]);
	printf("%p\n",arr[2]);

	printf("%p\n",&arr[0][2]);   
	printf("%p\n",&arr[1][1]);
	printf("%p\n",&arr[2][0]);
}
