#include<stdio.h>
int main(){
	int a[5]={10};  //here array a is initialize of size 5
	printf("%d\n",a[3]);//only one value is kept that is 10 
	// array index starts with zero 
	printf("%d\n",a[4]);//when only one value is put in the array it is equivalent to{10,0,0,0,0}
	printf("%d\n",a[0]);//this 0 depends on type of array if array is float you get{10,0.0,0.0}
	printf("%d\n",a[1]);
	printf("%d\n",a[5]);//when we try to access element out of array we get CV

	return 0;
}


