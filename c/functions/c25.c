//function pointer / pointer to function
#include<stdio.h>
void add(int a,int b){
	printf("add:%d\n",a+b);
}void sub(int a,int b){
	printf("sub:%d\n",a-b);
}void main(){
	void (*ptr)(int,int)=NULL;//function pointer
	ptr=add;
	ptr(10,20);
	ptr=sub;
	ptr(30,10);
}

