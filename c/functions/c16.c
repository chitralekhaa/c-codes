//call by value
#include<stdio.h>
void swap(int,int);
void main(){
	int x=10;
	int y=20;
	printf("%d\n",x);
	printf("%d\n",y);
	swap(x,y);
	printf("x=%d\n",x);
	printf("y=%d\n",y);
}void swap(int x,int y){
	printf("%d\n",x);
	printf("%d\n",y);
	int temp;
	temp=x;
	x=y;
	y=temp;
	printf("x=%d\n",x);
	printf("y=%d\n",y);
}

