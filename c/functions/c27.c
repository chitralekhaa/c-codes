/* function pointer arithmetic */

#include<stdio.h>
void add(int a,int b){
	printf("add1:%d\n",a+b);
	printf("add2:%d\n",a+b);
	printf("add3:%d\n",a+b);
}void main(){
	void (*ptr)(int ,int)=NULL;
	ptr=add;
	ptr(10,20);
	printf("ptr:%p\n",ptr);
	ptr++;
	printf("ptr:%p\n",ptr);
	ptr(30,40);
	ptr++;//we can perform max 0ne incre op on func pointer after that we get illegal instruction
	printf("ptr:%p\n",ptr);
	ptr(50,60);
}


