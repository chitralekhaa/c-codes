//call by address (by reference)
#include<stdio.h>
void callbyref(int *ptr);  //pointer can see across the stack frame
void main(){
	int x=10;
	printf("%d\n",x);
	callbyref(&x);
	printf("%d\n",x);
}void callbyref(int *ptr){
	printf("%p\n",ptr);
	printf("%d\n",*ptr);
	*ptr=50;
	printf("%d\n",*ptr);
}
