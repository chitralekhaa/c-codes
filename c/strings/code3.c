#include<stdio.h>
void main(){
	char carr[]={'c','o','r','e','2','w','e','b'};
	char *str="core2web";
	printf("%s\n",carr);
	printf("%s\n",str);
	printf("%c\n",*(str+1));  // we can read the character from constant string
	carr[4]='3';
	printf("%s\n",carr);   // we can change character from initializr list o/p is core3web
	*str='B';
	printf("%s\n",str);// we cannot change or replace character form constant string ,string constant gets stored in RO data section
	// segmentation fault (You are trying to access illegal address)
}
