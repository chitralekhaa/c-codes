//nested structure
#include<stdio.h>
#include<string.h>
struct movieInfo{
	float IMBD;
	char actor[10];
};
struct movie{
	char mName[20];
	struct movieInfo obj1;
};
void main(){
	struct movie obj2;
	strcpy(obj2.mName,"Kantar");
	strcpy(obj2.obj1.actor,"rishabh");
	obj2.obj1.IMBD=9.7;
	printf("%s\n",obj2.mName);
	printf("%s\n",obj2.obj1.actor);
	printf("%f\n",obj2.obj1.IMBD);
}


