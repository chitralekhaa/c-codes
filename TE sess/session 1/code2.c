/* print numbers divisible by 3 and 7 in given range in given order */
#include<stdio.h>
void main(){
	int x,y;
	printf("enter range ");
	scanf("%d %d",&x,&y);
	for(int i=y;i>=x;i--){
		if(i%3==0 && i%7==0){
			printf("%d\n",i);
		}
	}
}
