/*WAP and print the values of below expressions
 x=9
 ans= ++x + x++ + ++x
 ans1=++x + ++x + ++x + ++x
 ans2= x++ + x++ + ++x + x++ + ++x
 ans3=x++ + x++ + x++ + x++
 */ 



#include<stdio.h>
void main(){
	int x=9;
	int ans,ans1,ans2,ans3;

	ans= ++x + x++ + ++x;
	printf("x = %d\n ++x + x++ + ++x = %d\n",x,ans);

	ans1=++x + ++x + ++x + ++x;
	printf("x = %d\n ++x + ++x + ++x + ++x = %d\n",x,ans1);

	ans2= x++ + x++ + ++x + x++ + ++x;
	printf("x = %d\n x++ + x++ + ++x + x++ + ++x = %d\n",x,ans2);

	ans3=x++ + x++ + x++ + x++;
	printf("x = %d\n x++ + x++ + x++ + x++ = %d\n",x,ans3);
}
