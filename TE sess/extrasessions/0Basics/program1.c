/*WAP to print value and size of below variables.Take all the values from the user
 num=10
 chr='S'
 rs=55.20
 crMoney=3892678935759.89436776*/



#include<stdio.h>
void main(){
	int num;
	char chr;
	float rs;
	double crMoney;
	
        scanf("%d",&num);	
	printf("Enter an integer:%d\n",num);
	printf("Size of %d is:%ld\n\n",num,sizeof(num));

        scanf(" %c",&chr);
	printf("Enter a character:%c\n",chr);
	printf("Size of %c is:%ld\n\n",chr,sizeof(chr));

	scanf("%f",&rs); 
        printf("Enter a float:%f\n",rs);
	printf("Size of %f is:%ld\n\n",rs,sizeof(rs));

	scanf("%lf",&crMoney);
	printf("%lf\n",crMoney); 
	printf("Size of %lf is:%ld\n\n",crMoney,sizeof(crMoney));
}
