/* WAP to the array elements in reverse order */
#include<stdio.h>
void main(){
	int size;
	printf("enter size:");
	scanf("%d",&size);
	int arr[size];
	for(int i=0;i<size;i++){
		scanf("%d",&(arr[i]));
	}printf("array in reverse order:\n");
	for(int i=size-1;i>=0;i--){
		printf("%d\n",arr[i]);
	}
}
