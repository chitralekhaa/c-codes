/* WAP to swap values of two numbers using pointer */
#include<stdio.h>
void main(){
	int x,y,t;
	printf("enter x value:\n");
	scanf("%d",&x);
	printf("enter y value:\n");
	scanf("%d",&y);
	int *px=&x;
	int *py=&y;
	t=*px;
	*px=*py;
	*py=t;
	printf("After swapping\n");
	printf("x=%d\n",*px);
	printf("y=%d\n",*py);
}

