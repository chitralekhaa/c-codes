/* 1   2   3   4
 * 25  36  49  64
 * 9   10  11  12
 * 169 196 225 256*/
#include<stdio.h>
void main(){
	int n;
	printf("enter no of rows ");
	scanf("%d",&n);
	int a=1;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			if(i%2!=0){
				printf(" %2d ",a);
				a++;
			}else{
				printf(" %2d ",a*a);
				a++;
			}
		}printf("\n");
	}
}

