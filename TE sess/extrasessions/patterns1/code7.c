/*  1   2  9    4  
 *  25  6  49   8
 *  81  10 121  12
 *  169 14 225  16*/
#include<stdio.h>
void main(){
	int n;
	printf("enter no of rows ");
	scanf("%d",&n);
	int a=1;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			if(j%2!=0){
			printf(" %2d ",a*a);
			a++;
			}else{
				printf(" %2d ",a);
				a++;
			}
		}printf("\n");
	}
}
