//WAP that dynamically allocates 1D array of marks take value from user and print it
#include<stdio.h>
#include<stdlib.h>
void main(){
	int size=5;
	int *ptr=(int*)malloc(size*sizeof(int));
	printf("enter array elements");
	for(int i=0;i<size;i++){
		scanf("%d",ptr+i);
	}printf("array elements are");
	for(int i=0;i<size;i++){
		printf("%d\n",*(ptr+i));
	}
	free(ptr);
}
